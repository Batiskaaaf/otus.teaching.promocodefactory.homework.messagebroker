using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.Pcf.Administration.WebHost.Logging
{
    public class ConsumeObserver : IConsumeObserver
    {
        private readonly ILogger<ConsumeObserver> logger;

        public ConsumeObserver(ILogger<ConsumeObserver> logger)
        {
            this.logger = logger;
        }
        public Task ConsumeFault<T>(ConsumeContext<T> context, Exception exception) where T : class
        {
            logger.LogWarning("Error while consuming event: {eventId}", context.MessageId);
            return Task.CompletedTask;
        }

        public Task PostConsume<T>(ConsumeContext<T> context) where T : class
        {
            logger.LogInformation("Event consumed succesfully: {eventId}", context.MessageId);
            return Task.CompletedTask;
            
        }

        public Task PreConsume<T>(ConsumeContext<T> context) where T : class
        {
            logger.LogInformation("Start to consume event: {eventId}", context.MessageId);
            return Task.CompletedTask;
        }
    }
}