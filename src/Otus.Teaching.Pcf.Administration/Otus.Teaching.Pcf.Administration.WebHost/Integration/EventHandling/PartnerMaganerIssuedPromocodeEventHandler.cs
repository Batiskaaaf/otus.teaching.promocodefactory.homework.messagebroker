using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Abstract;
using Otus.Teaching.Pcf.IntegrationEvents;

namespace Otus.Teaching.Pcf.Administration.WebHost.Integration.EventHandling
{
    public class PartnerMaganerIssuedPromocodeEventHandler : IConsumer<PartnerMaganerIssuedPromocodeEvent>
    {
        private readonly IEmployeesService employeesService;

        public PartnerMaganerIssuedPromocodeEventHandler(IEmployeesService employeesService)
        {
            this.employeesService = employeesService;
        }

        public async Task Consume(ConsumeContext<PartnerMaganerIssuedPromocodeEvent> context)
        {
            await employeesService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}