using System;

namespace Otus.Teaching.Pcf.IntegrationEvents
{
    public class PartnerMaganerIssuedPromocodeEvent
    {
        public PartnerMaganerIssuedPromocodeEvent(Guid partnerManagerId)
        {
            PartnerManagerId = partnerManagerId;
        }
        public Guid PartnerManagerId { get; }
    }
}
