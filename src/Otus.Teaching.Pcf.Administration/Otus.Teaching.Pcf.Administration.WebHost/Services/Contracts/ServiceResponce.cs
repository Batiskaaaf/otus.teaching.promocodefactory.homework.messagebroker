using System.Collections.Generic;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services.Contracts
{
    public class ServiceResponse<T>
    {
        public T Data { get; set; }
        public bool Success { get; set; } = true;
        public string Message { get; set; } = null;
    }
}
