using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;
using Otus.Teaching.Pcf.IntegrationEvents;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstract;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Integration.EventHandling
{
    public class PromoCodeToCustomerRequestedEventHandler : IConsumer<PromoCodeToCustomerRequestedEvent>
    {
        private readonly IPromoCodesService promoCodesService;
        private readonly IMapper mapper;

        public PromoCodeToCustomerRequestedEventHandler(IPromoCodesService promoCodesService, IMapper mapper)
        {
            this.promoCodesService = promoCodesService;
            this.mapper = mapper;
        }
        public async Task Consume(ConsumeContext<PromoCodeToCustomerRequestedEvent> context)
        {   
            var givePromoCodeDTO = mapper.Map<GivePromoCodeDTO>(context.Message);
            await promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(givePromoCodeDTO);
        }
    }
}