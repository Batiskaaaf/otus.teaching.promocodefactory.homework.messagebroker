namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Configurations
{
    public class RabbitMqConfigurations
    {
        public string Uri { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}