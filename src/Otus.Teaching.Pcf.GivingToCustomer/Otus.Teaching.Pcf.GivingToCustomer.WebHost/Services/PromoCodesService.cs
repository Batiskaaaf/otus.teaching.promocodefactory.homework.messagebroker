using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstract;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class PromoCodesService : IPromoCodesService
    {
        private readonly IRepository<PromoCode> promoCodesRepository;
        private readonly IRepository<Preference> preferencesRepository;
        private readonly IRepository<Customer> customersRepository;
        private readonly IMapper mapper;
        private readonly ILogger<PromoCodesService> logger;

        public PromoCodesService(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository,
            IMapper mapper,
            ILogger<PromoCodesService> logger)
        {
            this.promoCodesRepository = promoCodesRepository;
            this.preferencesRepository = preferencesRepository;
            this.customersRepository = customersRepository;
            this.mapper = mapper;
            this.logger = logger;
        }

        public async Task<ServiceResponse<bool>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeDTO givePromoCodeDTO)
        {
            var response = new ServiceResponse<bool>();
            //Получаем предпочтение по имени
            var preference = await preferencesRepository.GetByIdAsync(givePromoCodeDTO.PreferenceId);

            if (preference == null)
            {
                response.Success = false;
                response.Message = "Not found";
                return response;
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromDTO(givePromoCodeDTO, preference, customers);

            await promoCodesRepository.AddAsync(promoCode);

            response.Success = true;
            response.Message = "Ok";
            response.Data = true;


            return response;
        }
    }
}