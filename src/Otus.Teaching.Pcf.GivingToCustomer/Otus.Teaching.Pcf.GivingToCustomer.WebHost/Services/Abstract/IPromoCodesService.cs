using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstract
{
    public interface IPromoCodesService
    {
        Task<ServiceResponse<bool>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeDTO givePromoCodeDTO);
    }
}