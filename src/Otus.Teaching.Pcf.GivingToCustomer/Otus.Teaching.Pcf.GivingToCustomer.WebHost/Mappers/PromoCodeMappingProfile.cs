using AutoMapper;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;
using Otus.Teaching.Pcf.IntegrationEvents;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMappingProfile : Profile
    {
        public PromoCodeMappingProfile()
        {
            CreateMap<GivePromoCodeRequest, GivePromoCodeDTO>();
            CreateMap<PromoCodeToCustomerRequestedEvent, GivePromoCodeDTO>();
        }
    }
}