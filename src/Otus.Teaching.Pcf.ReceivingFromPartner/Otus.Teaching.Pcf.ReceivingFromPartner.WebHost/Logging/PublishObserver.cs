using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Logging
{
    public class PublishObserver : IPublishObserver
    {
        private readonly ILogger<PublishObserver> logger;

        public PublishObserver(ILogger<PublishObserver> logger)
        {
            this.logger = logger;
        }
        public Task PostPublish<T>(PublishContext<T> context) where T : class
        {
            logger.LogInformation("Event published succesfully: {eventId}", context.MessageId);
            return Task.CompletedTask;
        }

        public Task PrePublish<T>(PublishContext<T> context) where T : class
        {
            logger.LogInformation("Publishing event: {eventId}", context.MessageId);
            return Task.CompletedTask;
        }

        public Task PublishFault<T>(PublishContext<T> context, Exception exception) where T : class
        {
            logger.LogWarning("Error while publishing event, {eventId}. Error: {errorMsg}", context.MessageId, exception.Message);
            return Task.CompletedTask;
        }
    }
}